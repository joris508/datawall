/**
 * Created by Joris on 13/06/16.
 */
var w, h, nx, ny, dx, dy, imgdata;
var canvas = document.getElementById("scene");
var ctx = canvas.getContext("2d");
var particles = [];
var numberOfPeople = 0;
var finetuneDivider = 0;
var stage = new createjs.Stage("scene");
var img = new Image();
img.src = "img/mansilhouette2.png";
var imgX = window.innerWidth / 2 - img.width / 2;
var imgY = window.innerHeight / 2 - img.height / 2;

function init(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    ctx.drawImage(img, imgX, imgY);
    imgdata = ctx.getImageData(imgX, imgY, img.width, img.height);
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    backUp();
}

function calculate(){
    //Clear the canvas and the arrays;
    clearEverything();

    //The calculation is not perfect, so there's a variable that needs to be adjusted at some points.
    if(numberOfPeople < 50){
        finetuneDivider = 1.65;
    } else if (numberOfPeople < 300) {
        finetuneDivider = 1.55;
    } else if (numberOfPeople < 800) {
        finetuneDivider = 1.45;
    } else {
        finetuneDivider = 1.35;
    }

    w = img.width/finetuneDivider;
    h = img.height/finetuneDivider;

    nx = Math.sqrt((w / h) * numberOfPeople + Math.pow(w - h, 2) / (4 * Math.pow(h, 2))) - (w - h) / (2 * h);
    ny = numberOfPeople / nx;

    dx = Math.floor(w / nx);
    dy = Math.floor(h / ny);

    //Loop through the image data and store the necessary particles
    var counter = 0;
    for (var y = 0; y < imgdata.height; y+=dy){
        for (var x = 0; x < imgdata.width; x+=dx){
            var color = "rgb(" + imgdata.data[(y * 4 * imgdata.width) + (x * 4)] + "," + imgdata.data[(y * 4 * imgdata.width) + (x * 4) + 1] + "," + imgdata.data[(y * 4 * imgdata.width) + (x * 4) + 2] + ")";
            if (color != "rgb(0,0,0)") {
                var randomColor = Math.ceil((Math.random() * 200) + 50);
                randomColor = randomColor.toString();
                if (counter < particles.length){
                    var oldX = particles[counter].x;
                    var oldY = particles[counter].y;
                    particles[counter] = {
                        x: x * (Math.random() * 0.1 + 1.9) + 750,
                        y: y * (Math.random() * 0.1 + 1.9) + 190,
                        oldX: oldX,
                        oldY: oldY,
                        color: "rgb(" + randomColor + "," + randomColor + "," + randomColor + ")",
                        size: (dx * 0.25) + (Math.random() * dx * 0.5)
                    };
                }
                counter++;
            }
        }
    }

    //Generate circles on easelJS stage for every particle in the particles array.
    render();
}

function clearEverything(){
    if (particles.length != numberOfPeople){
        reformParticlesArray();
    }
    stage.removeAllChildren();
    stage.update();
}

function reformParticlesArray(){
    var difference;
    if (particles.length > numberOfPeople) {
        difference = particles.length - numberOfPeople;
        particles.splice(numberOfPeople, difference);
    } else if (particles.length < numberOfPeople) {
        difference = numberOfPeople - particles.length;
        for (var i = 0; i < difference; i++){
            var randomPosition = Math.floor(Math.random()*(window.innerWidth+1));
            var particle = {
                x: randomPosition,
                y: 0
            };
            particles.push(particle);
        }
    }
}

//function getApiData(){
//    var api = "http://145.93.32.38:8080/datawall/api/presence/count";
//    $.getJSON (api, {
//        })
//        .done(function(data){
//            if (numberOfPeople != data) {
//                numberOfPeople = data;
//                calculate();
//            }
//        });
//}

function backUp(){
    document.addEventListener("keypress", function(){
        numberOfPeople = numberOfPeople + 100;
        calculate();
    });

    document.addEventListener("click", function(){
        numberOfPeople = numberOfPeople - 100;
        if (numberOfPeople < 0){
            numberOfPeople = 0;
        }
        calculate();
    });
}

function render() {
    for (var i = 0; i < particles.length; i++) {
        var randomX = Math.ceil(Math.random() * 40);
        var randomY = Math.ceil(Math.random() * 40);
        var particle = particles[i];
        var differenceX = particle.x - particle.oldX;
        var differenceY = particle.y - particle.oldY;

        var circle = new createjs.Shape();
        circle.graphics.beginFill(particle.color).drawCircle(particle.oldX, particle.oldY, particle.size);
        stage.addChild(circle);
        //This animates the particles from old coördinates to the new ones
        createjs.Tween.get(circle)
            .to({x: differenceX, y: differenceY}, 1500, createjs.Ease.getPowInOut(2))
            .to({x: differenceX + randomX, y: differenceY + randomY}, 4000)
            .to({x: differenceX, y: differenceY}, 4500);

        createjs.Tween.get(circle, {loop: true})
            .to({x: differenceX, y: differenceY}, 0)
            .to({x: differenceX + randomX, y: differenceY + randomY}, 5000)
            .to({x: differenceX, y: differenceY}, 5000);


    }
    //Set framerate, this has a huge impact on performance obviously.
    createjs.Ticker.setFPS(30);
    createjs.Ticker.addEventListener("tick", stage);
}

//setInterval(function(){
//    getApiData();
//}, 10000);
