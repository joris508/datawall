Synopsis
---------
---------

This project is a datavisual in the open space on the third floor of Fontys R1. The project will visualize data, mostly gathered ourselves, from the occupants of R1 or the building itself. We strive to make a dynamic, or maybe an interactive datavisual.